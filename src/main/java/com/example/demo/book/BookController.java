package com.example.demo.book;

import com.example.demo.example.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * http://localhost:8080/swagger-ui.html
 * http://localhost:8080/books
 * http://localhost:8080/books?id=1
 */
@RestController
public class BookController {
    private HashMap<Integer, Author> authors = new HashMap<>();
    private HashMap<Integer, Book> books = new HashMap<>();

    private int authorId = 1;
    private int bookId = 1;
    {
        Author author1 = new Author(authorId++, "Author 1");
        Author author2 = new Author(authorId++, "Author 2");

        Book book1 = new Book(bookId++, "Title 1", author1);
        Book book2 = new Book(bookId++, "Title 2", author1);
        Book book3 = new Book(bookId++, "Title 3", author2);

        authors.put(author1.getId(), author1);
        authors.put(author2.getId(), author2);

        books.put(book1.getId(), book1);
        books.put(book2.getId(), book2);
        books.put(book3.getId(), book3);
    }


}
